import React from 'react';
import renderer from 'react-test-renderer';
import Table from './table';

import data from './__fixtures__/data.json';
import cols from './__fixtures__/cols.json';

describe('Table', () => {
    it('has base state', () => {
        const tree = renderer
            .create(<Table data={data} cols={cols} />)
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
})