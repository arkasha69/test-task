import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class Table extends PureComponent {
    static propTypes = {
        data: PropTypes.arrayOf(PropTypes.object),
        cols: PropTypes.arrayOf(PropTypes.shape({
            key: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired
        }))
    }

    render () {
        const { data, cols } = this.props;

        return (
            <table>
                <thead>
                    <tr>
                        {cols.map(({ name, key }) => <td key={key}>{name}</td>)}
                    </tr>
                </thead>
                <tbody>
                    {data.map((row, index) => (
                        <tr key={index}>
                            {cols.map(({ key }) => <td key={key}>{row[key]}</td>)}
                        </tr>
                    ))}
                </tbody>
            </table>
        );
    }
}