import React, { PureComponent } from 'react';
import Table from './components/table/table';

const DELAY = 3000;

const cols = [{
    key: 'symbol',
    name: 'Symbol name'
}, {
    key: 'ask',
    name: 'Ask'
}, {
    key: 'bid',
    name: 'Bid'
}, {
    key: 'direction',
    name: 'Direction'
}];

export default class App extends PureComponent {
    constructor(props) {
        super(props);

        this.getQuotes();

        this.state = {
            quotes: null
        };
    };

    render() {
        const { quotes } = this.state;

        if (quotes === null) {
            return null;
        }

        return (
            <Table data={quotes} cols={cols} />
        );
    }

    /**
     * Get quoates
     * @private
     */
    getQuotes() {
        fetch('https://quotes.instaforex.com/api/quotesTick?apiVersion=1&m=json')
            .then(response => {
                if (!response.ok) {
                    return Promise.reject();
                }
                return response.json()
            })
            .then(data => data.map(data => ({
                ...data,
                direction: data.change = 0 ? '-' : data.change > 0 ? 'up' : 'down'
            })))
            .then(data => {
                this.setState({ quotes: data });
            })
            .catch(() => {})
            .then(() => setTimeout(() => this.getQuotes(), DELAY));
    }
}
